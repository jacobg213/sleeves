$(document).ready(function () {
    toggle_dropdown('.shopping-cart', '.open-shopping-cart');
    recommendations();
    /// modals
    increase_product_quantity('.modal-add-to-cart', '.quantity-more', '.quantity');
    decrease_product_quantity('.modal-add-to-cart', '.quantity-less', '.quantity');
    open_mobile_nav();
    open_mobile_cart();
    open_modals();

    open_checkout();
    smooth();
    validate_messages();
    validate_checkout();
    add_to_cart();
    remove_from_cart();
    make_ajax_order_request();
    increse_quantity_cart();
    summary_cart();
    cancel_checkout();
});


function summary_cart(){
    $('.summ-up').on('click', function() {
        create_summary_info_for_checkout();
    })
}

function cancel_checkout() {
    $('.go-step-back-to-shopping').on('click', function () {
        $('#checkout-modal').remodal().close();
    })
    
}


function toggle_dropdown(buttonSelector, iconSelector) {
    $('body').on('click', iconSelector, function (e) {
        var $this = $(this);
        e.preventDefault();
        if ($('.is-open').length > 0 && $('.is-open')[0] != $this.parents(buttonSelector)[0]) {
            $('.is-open').removeClass('is-open');
        }
        $this.parents(buttonSelector).toggleClass('is-open');
        e.stopPropagation();
    });
    $('body').on('click', '.cart-details', function (e) {
        e.stopPropagation();
    });
    close_dropdown(buttonSelector);
}



function close_dropdown(buttonSelector) {
    $("body").on('click', function () {
        $(buttonSelector).removeClass("is-open");
    });
}


function product_search(games) {

    $('.search-tool .clear-filter').on('click', function () {
        $('.product').fadeIn();
        $('#product-search').val('');
        $(this).prop("disabled", true);
    });

    $('#product-search').on('change textInput input', function () {
        if (!$(this).val() != '') {
            $('.product').fadeIn();
            $('#product-search').val('');
            $('.search-tool .clear-filter').prop("disabled", true);
        }
    });

    $(function () {
        $("#product-search").autocomplete({
            minLength: 0,
            source: games,
            appendTo: ".search-tool-list",
            focus: function (event, ui) {
                return false;
            },
            select: function (event, ui) {
                $('.product').hide();
                $.each(ui.item.sleeve_id, function (index, value) {
                    $('.product[data-size="' + value + '"]').fadeIn();
                })
                $('.search-tool .clear-filter').prop("disabled", false);
                $('#product-search').val(ui.item.value);
                return false;
            }

        })
    });
}


function recommendations() {
    $('.recommendations-slider').owlCarousel({
        loop: true,
        nav: false,
        autoplay: true,
        items: 1
    })
}

function increase_product_quantity(wrapper, button, input) {
    $(wrapper).on('click', button, function () {
        var val = $(this).parents(wrapper).find(input).val();
        $(this).parents(wrapper).find(input).val(parseInt(val) + 1)
    })
}

function decrease_product_quantity(wrapper, button, input) {
    $(wrapper).on('click', button, function () {
        var val = $(this).parents(wrapper).find(input).val();
        if (val > 1) {
            $(this).parents(wrapper).find(input).val(parseInt(val) - 1);
        }
    })
}


function open_mobile_nav() {
    $('.open-menu').on('click', function () {
        $('.main-navigation').slideToggle();
    })
}

function open_mobile_cart() {
    $('.mobile-cart-open').on('click', function () {
        $('.cart-details.mobile').slideToggle();
    })
}

function open_modals() {
    $('.products-wrapper').on('click', '.product .product-image', function () {
        var ref = $(this).parents('.product').attr('data-modal-ref');
        $('[data-remodal-id=' + ref + ']').remodal().open();
    });
}

function open_checkout() {
    var open = 0;
    $('.checkout-button').on('click', function () {
        var ref = $(this).parents('.product').attr('data-modal-ref');
        var cart = getCart();
        cart.done(function (cart_object) {
            var i = 1;
            $.each(cart_object.Lines, function (index, line) {
                $('.checkout-slider table tr:not(:first):not(:last)').remove();
                setTimeout(function () {
                    var modal_table_element = '<tr><td>' + i + '</td><td>' + line.Product.Title + '</td><td>' + line.Quantity + '</td><td>' + line.Price + ' dkk</td></tr>';
                    $('.checkout-slider').find('table tr:last-child').before(modal_table_element);
                    products += line.Quantity;
                    i += 1;
                    $('.checkout-summary').remove();
                    $('.checkout-slider').find('table').append('<tr class="checkout-summary"><th colspan="3">Total price:</th><th>'+cart_object.TotalPrice+' dkk</th></tr>')
                }, 10)
            })
            if(cart_object.Lines.length > 0){
                var options = {
                    closeOnEscape: false,
                    closeOnOutsideClick: false
                };
                if (open == 0) {
                    checkout_slider();
                }
                $('[data-remodal-id="checkout-modal"]').remodal(options).open();

            } else {
                alert('Your cart is empty, you cannot checkout without products :)')
            }
        });



     

    });
}

/// smooth scrolling

function smooth() {
    $(function () {
        $('a[href*="#"]:not([href="#"])').click(function () {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') && location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (this.hash == '#top') {
                    $('html, body').animate({
                        scrollTop: 0
                    }, 1000);
                    return false;
                } else {
                    if (target.length) {
                        $('html, body').animate({
                            scrollTop: target.offset().top - 100
                        }, 1000);
                        return false;
                    }
                }
            }
        });
    });
}

function checkout_slider() {
    var checkout_slider = $('.checkout-slider');
    checkout_slider.owlCarousel({
        loop: false,
        nav: false,
        items: 1,
        animateOut: 'fadeOut',
        animateIn: 'fadeIn',
        mouseDrag: false,
        touchDrag: false,
        pullDrag: false,
        autoHeight: true,
        smartSpeed: 100,
        fluidSpeed: 100,
        onChanged: function (event) {
            var index = (event.item.index + 1);
            $('.step').removeClass('active');
            $('.step:nth-child(' + index + ')').prevAll('.step').addClass('active');
            $('.step:nth-child(' + index + ')').addClass('active');
        }
    });


    $('.checkout-slider').on('click', '.go-to-next-step', function () {
        var form = $(this).parents('.item-wrapper').children('form');
        var data_step = $(this).attr('data-step');
        if (form.length > 0) {
            form.submit();
            if (form.find('input.error').length == 0) {
                checkout_slider.trigger('to.owl.carousel', data_step);
            }
        } else {
            checkout_slider.trigger('to.owl.carousel', data_step);
        }
    });

    $('.checkout-slider').on('click', '.go-step-back', function () {
        checkout_slider.trigger('prev.owl.carousel');
    });

}




function validate_checkout() {

    $("form").submit(function (e) {
        e.preventDefault();
    });

    $("#delivery").validate({
        rules: {
            user_email: {
                required: true,
                email: true
            }
        }
    });

    $("#payment").validate({
        rules: {
            card_number: {
                required: true,
                digits: true
            },
            ccv: {
                required: true,
                digits: true
            }
        }
    });
}

function validate_messages() {
    jQuery.extend(jQuery.validator.messages, {
        required: "This field is required.",
        remote: "Please fix this field.",
        email: "Please enter a valid email address.",
        url: "Please enter a valid URL.",
        date: "Please enter a valid date.",
        dateISO: "Please enter a valid date (ISO).",
        number: "Please enter a valid number.",
        digits: "Please enter only digits.",
        creditcard: "Please enter a valid credit card number.",
        equalTo: "Please enter the same value again.",
        accept: "Please enter a value with a valid extension.",
        maxlength: jQuery.validator.format("Please enter no more than {0} characters."),
        minlength: jQuery.validator.format("Please enter at least {0} characters."),
        rangelength: jQuery.validator.format("Please enter a value between {0} and {1} characters long."),
        range: jQuery.validator.format("Please enter a value between {0} and {1}."),
        max: jQuery.validator.format("Please enter a value less than or equal to {0}."),
        min: jQuery.validator.format("Please enter a value greater than or equal to {0}.")
    });
}

function add_to_cart() {
    $('.add-to-cart-req').on('click', function () {
        var qty = 1;
        if ($(this).parents('.remodal').find('.quantity').length > 0) {
            qty = $(this).parents('.remodal').find('.quantity').val();
        }
        var id = $(this).parents('.product').attr('data-product-id');
        make_ajax_request(id, qty, '/cart/addtocart');
        iziToast.show({
            title: '',
            color: '#fa6e41',
            position: 'topRight',
            iconColor: '#fff',
            timeout : 1500,
            icon: 'dripicons-checkmark',
            message: 'Your product has been added to cart :)'
        });
    });
}

function remove_from_cart() {
    $('.cart-product-list').on('click', '.remove-quantity', function () {
        var id = $(this).parents('.product-actions').siblings('.product-description').attr('data-product-id');
        make_ajax_request(id, null, '/cart/removefromcart', false);
    });
}


function increse_quantity_cart() {
    $('.cart-product-list').on('click', '.add-quantity', function () {
        var id = $(this).parents('.product-actions').siblings('.product-description').attr('data-product-id');
        make_ajax_request(id, null, '/cart/addtocart');
    });
}
function CartHardClear() {
    var request = $.ajax({
        url: '/cart/clearcart',
        type: "POST",
        data: {},
        dataType: "json"
    });

    request.done(function (cart) {
        update_carts(cart);
    });

}


function getCart() {
    var request = $.ajax({
        url: '/cart/index',
        type: "POST",
        data: {},
        dataType: "json"
    });

    return request;

}


function make_ajax_request(product_id, quantity, target, hard) {

        var qty;
        if (quantity == null) {
            qty = 1;
        } else {
            qty = quantity
        }

        var hard_remove = false;
        if (hard != false) {
            hard_remove = true;
        }
        var request = $.ajax({
            url: target,
            type: "POST",
            data: { productId: product_id, quantity: qty, hard: hard_remove},
            dataType: "json"
        });

        request.done(function (msg) {
            update_carts(msg);
        });

        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
}


function update_carts(cart) {
    var cart_element = $('.cart-details');
    cart_element.find('.cart-product-list li').remove(); // remove old products
    // main cart strucutre
    var products = 0;
    $.each(cart.Lines, function (index, line) {
        var main_cart_element = '<li><span data-product-id="' + line.Product.Id + '" class="product-description">' + line.Product.Title + '</span><span class="product-actions"><button class="btn add-quantity"><i class="dripicons-plus"></i></button><button class="btn remove-quantity"><i class="dripicons-minus"></i></button><input type="number" class="current-quantity" value="' + line.Quantity + '"></span><span class="item-price">' + line.Price + ' dkk</span></li>';
        cart_element.find('.cart-product-list').append(main_cart_element);
        products+= line.Quantity;
    })
    if (products == 0) {
        cart_element.find('.cart-product-list').append('<li>Your cart is empty</li>');
        
    }

    $('.amount-of-products').text(products);
    cart_element.find('.total-price').text(cart.TotalPrice + ' dkk')
}

function make_ajax_order_request() {
    $('#confirm-pay').on('click', function () {
        if ($('.privacy input').is(':checked') == false) {
            alert('You must accept our rules and terms of use');
        }else{
        var request = $.ajax({
            url: "/Home/Order",
            type: "POST",
            data: {
                FirstName: get_value('#name'),
                LastName : get_value('#surname'),
                Street: get_value('#street'),
                Zip : get_value('#zip'),
                City : get_value('#city'),
                Country : get_value('#country'),
                Email : get_value('#user_email'),
                Phone : get_value('#phone')

            },
            dataType: "html"
        });
        request.done(function (msg) {
            if (msg == 'True') {
                CartHardClear();
                $('#checkout-modal').remodal().close();
                $('[data-remodal-id="success"]').remodal().open();
                $('.checkout-slider').trigger('to.owl.carousel', 0);
            }
        });

        request.fail(function (jqXHR, textStatus) {
            alert("Request failed: " + textStatus);
        });
        }
        });
   
}

function create_summary_info_for_checkout() {
    var cart = getCart();
    $('.order-detail ul li').remove();
    cart.done(function (cart_object) {

        $.each(cart_object.Lines, function (index, line) {
            $('.order-products ul').append('<li>' + line.Quantity + 'x ' + line.Product.Title + ' - ' + line.Price + ' dkk</li>')
        })
        $('.order-products .total-order-price > span').text(cart_object.TotalPrice + ' dkk')
    })

    $('.delivery-details ul').append('<li>Full name: ' + get_value('#name') + ' ' + get_value('#surname') + '</li><li>Street: ' + get_value('#street') + '</li><li>City: ' + get_value('#city') + '</li><li>Zip code: ' + get_value('#zip') + '</li><li>Country: ' + get_value('#country') + '</li>');

    $('.payment-details ul').append('<li>Payment type: Credit card</li><li>Card type: ' + get_value('#card') + '</li>');

    $('.contact-details ul').append('<li>Email address: ' + get_value('#user_email') + '</li><li>Phone number: ' + get_value('#phone') + '</li>');
}


function get_value(dom_selector) {
    return $(dom_selector).val();
}

