﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace FinalProject
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "Admin",
                url: "admin",
                defaults: new { controller = "Account", action = "Login" }
            );

            routes.MapRoute(
                name: "Products",
                url: "admin/products",
                defaults: new { controller = "Products", action = "Index" }
            );

            routes.MapRoute(
                name: "Games",
                url: "admin/games",
                defaults: new { controller = "Games", action = "Index" }
            );

            routes.MapRoute(
                name: "Invoices",
                url: "admin/invoices",
                defaults: new { controller = "Invoices", action = "Index" }
            );

            routes.MapRoute(
                name: "Default",
                url: "{controller}/{action}/{id}",
                defaults: new { controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
