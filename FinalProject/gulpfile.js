var gulp = require('gulp');
var sass = require('gulp-sass');
var plumber = require('gulp-plumber');


gulp.task('default', function(){
    return gulp.src('Content/sass/main.scss')
        .pipe(plumber())
        .pipe(sass()) // Converts Sass to CSS with gulp-sass
        .pipe(gulp.dest('Content/css'))
});


gulp.task('watch', function(){
    gulp.watch('Content/sass/**/*.scss', ['default']);
})