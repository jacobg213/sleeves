﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FinalProject.Models;
using System.ComponentModel.DataAnnotations;

namespace FinalProject.Models
{
    public class Product
    {
        public Product()
        {
            this.Games = new HashSet<Game>();
            Invoices = new HashSet<Invoice>();
        }

        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        public string Description { get; set; }
        [Required]
        public int Stock { get; set; }
        public string Color { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public string ImageUrl { get; set; }
        public int Price { get; set; }

        public ICollection<Game> Games { get; set; }
        public ICollection<Invoice> Invoices { get; set; }
    }
}