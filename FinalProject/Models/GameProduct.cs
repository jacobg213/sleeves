﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.Models
{
    public class GameProduct
    {
        public int Id { get; set; }
        public int GameId { get; set; }
        public int ProductId { get; set; }

        public Game Game { get; set; }
        public Product Product { get; set; }
    }
}