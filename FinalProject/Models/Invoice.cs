﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;
using Microsoft.Ajax.Utilities;

namespace FinalProject.Models
{
    public class Invoice
    {
        private DBConnection db = new DBConnection();
        public Invoice()
        {
            Products = new HashSet<Product>();
            DateCreated = DateTime.Now;
        }

        public int Id { get; set; }
        public int TotalPrice { get; set; }
        public bool Paid { get; set; }
        public int Discount { get; set; }
        public DateTime DateCreated { get; }

        [Required]
        public int AccountId { get; set; }

        [ForeignKey("AccountRefId")]
        public virtual Account Account { get; set; }

        public ICollection<Product> Products { get; set; }
    }
}