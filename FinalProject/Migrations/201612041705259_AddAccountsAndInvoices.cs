namespace FinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAccountsAndInvoices : DbMigration
    {
        public override void Up()
        {
            CreateTable(
               "dbo.Accounts",
               c => new
               {
                   Id = c.Int(nullable: false, identity: true),
                   FirstName = c.String(nullable: false, maxLength: 256),
                   LastName = c.String(nullable: false, maxLength: 256),
                   Email = c.String(nullable: false, maxLength: 256),
                   Phone = c.String(nullable: true, maxLength: 256),
                   Street = c.String(nullable: false, maxLength: 256),
                   City = c.String(nullable: false, maxLength: 256),
                   Zip = c.String(nullable: false, maxLength: 256),
                   Country = c.String(nullable: false, maxLength: 256),
                   Password = c.String(nullable: true, maxLength: 256),
               })
               .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Invoices",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    TotalPrice = c.Int(nullable: false),
                    Discount = c.Int(nullable: true),
                    Paid = c.Boolean(defaultValue: false),
                    AccountId = c.Int(nullable: false),
                    DateCreated = c.DateTime(nullable: false)
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.AccountId);

            AddForeignKey("dbo.Invoices", "AccountId", "dbo.Accounts", "Id");

            CreateTable(
                    "dbo.InvoiceProduct",
                    c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Amount = c.Int(nullable: false),
                        InvoiceId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.InvoiceId)
                .Index(t => t.ProductId);

            AddForeignKey("dbo.InvoiceProduct", "InvoiceId", "dbo.Invoices", "Id");
            AddForeignKey("dbo.InvoiceProduct", "ProductId", "dbo.Products", "Id");
        }
        
        public override void Down()
        {
            DropTable("dbo.Accounts");
            DropTable("dbo.Invoices");
            DropForeignKey("dbo.InvoiceProduct", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.InvoiceProduct", "ProductId", "dbo.Products");
        }
    }
}
