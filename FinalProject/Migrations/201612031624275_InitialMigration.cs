namespace FinalProject.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
               "dbo.Products",
               c => new
               {
                   Id = c.Int(nullable: false, identity: true),
                   Title = c.String(nullable: false, maxLength: 256),
                   Stock = c.Int(nullable: false),
                   Width = c.Int(nullable: false),
                   Height = c.Int(nullable: false),
                   Price = c.Int(nullable: false),
                   Color = c.String(nullable: false, maxLength: 256),
                   ImageUrl = c.String(nullable: false, maxLength: 256),
                   Description = c.String(nullable: true, maxLength: 20000)
               })
               .PrimaryKey(t => t.Id);

            CreateTable(
                "dbo.Games",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    Title = c.String(nullable: false, maxLength: 256),
                    ImageUrl = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id);

            CreateTable(
                    "dbo.GameProduct",
                    c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        GameId = c.Int(nullable: false),
                        ProductId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.GameId)
                .Index(t => t.ProductId);
            
            AddForeignKey("dbo.GameProduct", "GameId", "dbo.Games", "Id");
            AddForeignKey("dbo.GameProduct", "ProductId", "dbo.Products", "Id");
        }
        
        public override void Down()
        {
            DropTable("dbo.Products");
            DropTable("dbo.Games");
            DropForeignKey("dbo.GameProduct", "GameId", "dbo.Games");
            DropForeignKey("dbo.GameProduct", "ProductId", "dbo.Products");
            DropIndex("dbo.GameProduct", new[] { "GameId" });
            DropIndex("dbo.GameProduct", new[] { "ProductId" });
            DropTable("dbo.GameProduct");
        }
    }
}
