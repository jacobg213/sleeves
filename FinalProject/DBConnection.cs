using System.Data.Entity.ModelConfiguration.Conventions;
using FinalProject.Models;

namespace FinalProject
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class DBConnection : DbContext
    {
        public DBConnection()
            : base("name=DBConnection")
        {
        }

        public DbSet<Product> Products { get; set; }

        public DbSet<Game> Games { get; set; }

        public DbSet<Invoice> Invoices { get; set; }

        public DbSet<Account> Accounts { get; set; }

        public DbSet<InvoiceProduct> InvoiceProduct { get; set; }

        public DbSet<GameProduct> GameProduct { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Product>()
            .ToTable("Products")
            .HasMany(a => a.Games)
            .WithMany(a => a.Products)
            .Map(x =>
            {
                x.ToTable("GameProduct");
                x.MapLeftKey("ProductId");
                x.MapRightKey("GameId");
            });

            modelBuilder.Entity<Invoice>()
                    .ToTable("Invoices")
                    .HasRequired(s => s.Account)
                    .WithMany(s => s.Invoices)
                    .HasForeignKey(s => s.AccountId);

            modelBuilder.Entity<Product>()
            .HasMany(a => a.Invoices)
            .WithMany(a => a.Products)
            .Map(x =>
            {
                x.ToTable("InvoiceProduct");
                x.MapLeftKey("ProductId");
                x.MapRightKey("InvoiceId");
            });

            modelBuilder.Entity<Game>()
                .ToTable("Games")
                .HasMany(a => a.Products)
                .WithMany(a => a.Games)
                .Map(x =>
                {
                    x.ToTable("GameProduct");
                    x.MapLeftKey("GameId");
                    x.MapRightKey("ProductId");
                });

            modelBuilder.Entity<Account>().ToTable("Accounts");

            modelBuilder.Entity<InvoiceProduct>()
                .HasKey(c => new { c.ProductId, c.InvoiceId });

            modelBuilder.Entity<GameProduct>()
                .HasKey(c => new { c.ProductId, c.GameId });
        }
    }
}
