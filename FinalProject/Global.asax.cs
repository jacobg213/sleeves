﻿using FinalProject.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using FinalProject.Infrastructure.Binders;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace FinalProject
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            // Add custom model binder
            ModelBinders.Binders.Add(typeof(Cart), new CartModelBinder());
        }
    }
}
