﻿using FinalProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FinalProject.ViewModels
{
    public class Cart
    {

        private List<CartLine> lines = new List<CartLine>();

        public int TotalPrice
        {
            // Linq syntax
            get { return lines.Sum(e => e.Product.Price * e.Quantity); }
        }

        public List<CartLine> Lines { get { return lines; } }

        public Cart() { }

        public void AddItem(Product product, int quantity)
        {

            CartLine item = lines.Where(p => p.Product.Id == product.Id).FirstOrDefault();

            if (item == null)
            {
                lines.Add(new CartLine { Product = product, Quantity = quantity, Price = product.Price });
            }
            else
            {
                item.Quantity += quantity;
            }
        }

        public void RemoveItem(Product product, bool hard)
        {
            if(hard == true) {
                lines.RemoveAll(i => i.Product.Id == product.Id);
            }
            else {
                CartLine item = lines.Where(p => p.Product.Id == product.Id).FirstOrDefault();
                if (item != null)
                {
                    if (item.Quantity > 1) {
                        item.Quantity -= 1;
                    }else{
                        lines.RemoveAll(i => i.Product.Id == product.Id);
                    }
                }
            }
        }

        public void Clear()
        {
            lines.Clear();
        }
    }
}