﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using FinalProject.Models;
using FinalProject.ViewModels;
using FinalProject.Infrastructure.Binders;

namespace FinalProject.Controllers
{
    public class CartController : Controller
    {
        private DBConnection db = new DBConnection();
        private JavaScriptSerializer serializer = new JavaScriptSerializer();

        public string Index(Cart cart)
        {
            return serializer.Serialize(cart);
        }

        public string AddToCart(Cart cart, int productId, int quantity)
        {
            Product product = db.Products.Find(productId);
            if (product != null && quantity != 0)
            {
                cart.AddItem(product, quantity);
            }
            else if (product != null && quantity == 0)
            {
                cart.AddItem(product, 1);
            }
            else
            {
                return "failed";
            }

            return serializer.Serialize(cart); ;
        }


        public string RemoveFromCart(Cart cart, int productId, bool hard)
        {
            Product product = db.Products.Find(productId);

            if (product != null)
            {
                cart.RemoveItem(product, hard);
            }
            else
            {
                return "failed";
            }
            return serializer.Serialize(cart);
        }

        public string ClearCart(Cart cart)
        {
            cart.Clear();
            return serializer.Serialize(cart);
        }
    }

   
}