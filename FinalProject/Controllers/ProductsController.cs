﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalProject;
using FinalProject.Models;
using Microsoft.Ajax.Utilities;

namespace FinalProject.Controllers
{
    [Authorize]
    public class ProductsController : Controller
    {
        private DBConnection db = new DBConnection();

        // GET: Products
        public ActionResult Index()
        {
            return View(db.Products.Include("Games").ToList());
        }

        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = db.Products.Find(id);
            ViewBag.games = db.Games.Include("Products").ToList();
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // GET: Products/Create
        public ActionResult Create()
        {
            List<Game> games = db.Games.Include("Products").ToList();
            ViewBag.Games = games;
            return View();
        }

        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Title,Description,Stock,Color,Width,Height,ImageUrl,Price")] Product product, string[] selectedGames)
        {
            if (ModelState.IsValid)
            {
                db.Products.Add(product);
                UpdateProductProperties(selectedGames, product);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(product);
        }

        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = null;
            foreach (var pdt in db.Products.Include("Games").ToList())
            {
                if(pdt.Id == id)
                {
                    product = pdt;
                }
            } 
            if (product == null)
            {
                return HttpNotFound();
            }
            List<Game> games = db.Games.Include("Products").ToList();
            ViewBag.Games = games;
            return View(product);
        }

        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Title,Description,Stock,Color,Width,Height,ImageUrl,Price")] Product product, string[] selectedGames)
        {
            if (ModelState.IsValid)
            {
                try {
                    Product productWithGames = null;
                    foreach (var pdt in db.Products.Include("Games").ToList())
                    {
                        if (pdt.Id == product.Id)
                        {
                            productWithGames = pdt;
                        }
                    }
                    db.Entry(productWithGames).State = EntityState.Modified;
                    UpdateProductProperties(selectedGames, productWithGames);
                    db.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.Write(e.InnerException.Message);
                }
                return RedirectToAction("Index");
            }
            return View(product);
        }

        // GET: Products/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Product product = null;
            foreach (var pdt in db.Products.Include("Games").ToList())
            {
                if (pdt.Id == id)
                {
                    product = pdt;
                }
            }
            if (product == null)
            {
                return HttpNotFound();
            }
            return View(product);
        }

        // POST: Products/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Product product = null;
            foreach (var pdt in db.Products.Include("Games").ToList())
            {
                if (pdt.Id == id)
                {
                    product = pdt;
                }
            }
            product.Games.Clear();
            db.SaveChanges();
            db.Products.Remove(product);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private void UpdateProductProperties(string[] selectedGames, Product product)
        {
            if (selectedGames == null)
            {
                product.Games = new List<Game>();
                return;
            }

            var selectedPropertiessHs = new HashSet<string>(selectedGames);
            var productProperties = new HashSet<int>(product.Games.Select(c => c.Id));
            foreach (var game in db.Games)
            {
                if (selectedPropertiessHs.Contains(game.Id.ToString()))
                {
                    if (!productProperties.Contains(game.Id))
                    {
                        product.Games.Add(game);
                        game.Products.Add(product);
                        db.Entry(game).State = EntityState.Modified;
                    }
                }
                else
                {
                    if (productProperties.Contains(game.Id))
                    {
                        product.Games.Remove(game);
                        game.Products.Remove(product);
                        db.Entry(game).State = EntityState.Modified;
                    }
                }
            }
        }
    }
}
