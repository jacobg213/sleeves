﻿using FinalProject.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FinalProject.ViewModels;

namespace FinalProject.Controllers
{
    public class HomeController : Controller
    {
        private DBConnection db = new DBConnection();

        public ActionResult Index()
        {
            ViewBag.Products = db.Products.Include("Games").ToList();
            ViewBag.Games = db.Games.Include("Products").ToList();
            if ((Cart) Session["Cart"] == null)
            {
                ViewBag.Cart = new Cart();
            }
            else
            {
                ViewBag.Cart = (Cart) Session["Cart"];
            }
            return View();
        }

        [HttpPost]
        public bool Order([Bind(Include = "Id,FirstName,LastName,Email,Phone,Street,City,Zip,Country")] Account account)
        {
            if (ModelState.IsValid)
            {
                Cart cart = (Cart)Session["Cart"];
                db.Accounts.Add(account);
                Invoice invoice = new Invoice();
                invoice.Account = account;
                invoice.TotalPrice = cart.TotalPrice;
                foreach (CartLine line in cart.Lines)
                {
                    invoice.Products.Add(line.Product);
                }
                invoice.Paid = true;
                db.Invoices.Add(invoice);
                account.Invoices.Add(invoice);
                db.SaveChanges();
            }
            return true;
        }
    }
}