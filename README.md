### 1) Make sure you have running postres ###
### 2) Connect to database from pgadmin ###
### 3) Create new login role username "admin" password "admin" and remember to set is as superuser in 'Role Privilieges" tab ### 
### 4) Create new database "BoardSleeves" with owner set as admin ###
### 5) Go to https://github.com/npgsql/npgsql/releases and download "Npgsql-3.1.9.msi" and a bit lower "NpgsqlDdexProvider-3.1.0.vsix" ### 
### 6) Install downloaded installers ###
### 7) Download "Npgsql.dll" from https://drive.google.com/open?id=0B_6O0_rP56ODSzJ4WU1NT0s3R0E and put it in "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\PrivateAssemblies" ###
### 8) Edit "C:\Program Files (x86)\Microsoft Visual Studio 14.0\Common7\IDE\devenv.exe.config" devenv.exe.config and add these lines to line before </assemblyBinding>: ###
                <dependentAssembly>
                  <assemblyIdentity name="Npgsql" publicKeyToken="5d8b90d52f46fda7" culture="neutral" />
                  <bindingRedirect oldVersion="0.0.0.0-3.1.8.0" newVersion="3.1.8.0" />
                  <codeBase version="3.1.8.0" href="PrivateAssemblies\Npgsql.dll"/>
                </dependentAssembly>
### 9) Open project and run it ###
### 10) In any problem deleting the main applicationhost.config (in your "Documents/IIS Express" folder), check your solution folder for a hidden ".vs" folder with a "config" sub-folder. ###